// referencia https://www.teachmemicro.com/arduino-timer-interrupt-tutorial/

#define VERMELHO 10
#define AMARELO 9
#define VERDE 8
#define BOTAO 1

volatile int contador = 0;
volatile byte controle = LOW;

void setup() {
  pinMode(VERMELHO, OUTPUT);
  pinMode(AMARELO, OUTPUT);
  pinMode(VERDE, OUTPUT);
  Serial.begin(9600);

  // os bits de 2 a 0 manipulam o divisor do clock
  // com esta configuracao, a 07 faz com que divida por 1024
  // as outras op;'oes se encontram no link de ref
  // isso vai fazer que o overflow ocorra a cada 16.384 miliseg
  // em 61 overflows vai dar 1 seg
  TCCR2B = (TCCR2B & B11111000) | 0x07;

  // para abilitar o time overflow precisa-se setar o toie bit no timsk2
  TIMSK2 = (TIMSK2 & B11111110) | 0x01;

}

void loop() {

  //1 segundos
  if(contador == (61*2)){
    contador = 0;
    Serial.print("Deu");
    controle = !controle;
    digitalWrite(VERMELHO,controle);
  }

}

ISR(TIMER2_OVF_vect){
  contador = contador ++;
  controle = !controle;
  digitalWrite(VERMELHO,controle);
}
