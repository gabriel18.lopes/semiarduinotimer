//Referência: https://www.arduino.cc/reference/en/language/functions/communication/serial/
// https://www.arduino.cc/en/Tutorial/SerialEvent

#define VERMELHO 10
#define AMARELO 9
#define VERDE 8

volatile int controlador = 0;
const String senha = "1234";
int dadoUsuario;


// 0 senha incorreta
// 1 senha correta

String senhaEntrada = "";
bool pronto = false;  // Quando a string esta pronta



void setup() {
  pinMode(VERMELHO, OUTPUT);
  pinMode(AMARELO, OUTPUT);
  pinMode(VERDE, OUTPUT);

  // inicia comunicacao serial
  Serial.begin(9600);

  // reserva 200 bytes para senhaEntrada:
  senhaEntrada.reserve(200);
}

void loop() {

  if (pronto) {
     
    if(senhaEntrada.equals(senha)){

      Serial.print("Senha Correta \n");

      digitalWrite(VERMELHO,LOW);
      digitalWrite(VERDE,HIGH);
      delay(150);
      digitalWrite(VERDE,LOW);
      digitalWrite(AMARELO,HIGH);
      delay(150);
      digitalWrite(AMARELO,LOW);
      digitalWrite(VERMELHO,HIGH);
      delay(150);
      digitalWrite(VERMELHO,LOW);
      
    }else{
      
      Serial.print("Senha incorreta \n");
      digitalWrite(VERMELHO,HIGH);
    }

    // apaga a senha
    senhaEntrada = "";
    pronto = false;
  }

  
}

void serialEvent() {
  
  while (Serial.available()) {
    
    // le o que foi digitado um caracter por vez
    char testeSenha = (char)Serial.read();
    
    // se a entrada for uma nova linha manda de volta para o loop
    if (testeSenha == '\n') {
      pronto = true;
    }else{
      senhaEntrada += testeSenha;
    }
  }
  
}
